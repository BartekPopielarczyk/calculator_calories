import java.io.IOException;

import org.springframework.context.ApplicationContext;

import com.views.MainWindow;

public class App {

	public static void main(String[] args) throws IOException {
		ApplicationContext context = SpringBootstrap.buildContext();
		
		 MainWindow mainWindow = context.getBean("mainWindow", MainWindow.class);
			mainWindow.setVisible(true);
	}

}
