package com.models;

import com.common.ProductType;

public class Product {

	private int id;
	private String productName;
	private int calories;
	private int kilojoules;
	private ProductType type;
	private Boolean isSelected;

	public Product() {
		super();
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getCalories() {
		return calories;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	public int getKilojoules() {
		return kilojoules;
	}

	public void setKilojoules(int kilojoules) {
		this.kilojoules = kilojoules;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
