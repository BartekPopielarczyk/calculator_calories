package com.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.common.IProductAddedListener;
import com.models.Product;
import com.repository.IRepository;

@Component("mealsPanel")
public class MealsPanel extends JPanel implements IProductAddedListener {
	private JTable mealsTable;
	private ProductsTableModel mealsTableModel;
	private JButton addMealButton;
	private JButton deleteMealButton;
	private ChooseMealWindow chooseMealWindow;
	private JLabel totalCalories;
	private JLabel calories;
	private int totalCaloriesNumber = 0;
	private IRepository<Product> repository;

	@Autowired
	public MealsPanel(ChooseMealWindow chooseMealWindow, IRepository<Product> repository) {

		this.chooseMealWindow = chooseMealWindow;
		chooseMealWindow.addListener(this);
		this.repository = repository;
		setLayout(new BorderLayout());

		mealsTableModel = new ProductsTableModel();
		mealsTable = new JTable(mealsTableModel);
		mealsTable.setAutoResizeMode(WIDTH);
		addMealButton = new JButton("Add");
		addMealButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				chooseMealWindow.setVisible(true);
			}
		});
		deleteMealButton = new JButton("Delete");
		deleteMealButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Product product = mealsTableModel.get(mealsTable.getSelectedRow());
				mealsTableModel.remove(product);
				repository.Remove(product);
				totalCaloriesNumber -= product.getCalories();
				calories.setText(new Integer(totalCaloriesNumber).toString());
			}
		});

		JScrollPane scrollPane = new JScrollPane(mealsTable);
		scrollPane.setSize(new Dimension(2000, 500));
		add(scrollPane, BorderLayout.CENTER);

		totalCalories = new JLabel("TOTAL CALORIES: ");
		totalCalories.setBorder(new EmptyBorder(50, 50, 50, 50));
		calories = new JLabel("");

		Box box = Box.createHorizontalBox();
		box.add(addMealButton);
		box.add(deleteMealButton);
		box.add(totalCalories);
		box.add(calories);

		add(box, BorderLayout.SOUTH);

		Collection<Product> products = repository.Get();
		for (Product product : products) {
			totalCaloriesNumber += product.getCalories();
		}
		calories.setText(new Integer(totalCaloriesNumber).toString());
		mealsTableModel.replaceData(new ArrayList<Product>(products));
	}

	@Override
	public void actionPerformed(Product product) {
		chooseMealWindow.setVisible(false);
		totalCaloriesNumber += product.getCalories();
		calories.setText(new Integer(totalCaloriesNumber).toString());
		mealsTableModel.add(product);
		repository.Add(product);
	}
}
