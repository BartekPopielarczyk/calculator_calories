package com.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListDataListener;

import org.hibernate.jpa.event.internal.jpa.ListenerFactoryBeanManagerStandardImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.common.IProductAddedListener;
import com.common.MealType;
import com.models.Product;
import com.services.meat.IMeatStorage;
import com.services.vegitables.IVegitablesStorage;

@Component("chooseMealWindow")
public class ChooseMealWindow extends JFrame {

	private JComboBox mealsType;
	private MealsPanel panel;
	private JTable mealsTable;
	private JButton addButton;
	private ProductsTableModel mealsTableModel;
	private IMeatStorage meatStorage;
	private IVegitablesStorage vegitablesStorage;

	private ArrayList<IProductAddedListener> listeners = new ArrayList<>();

	@Autowired
	public ChooseMealWindow(IMeatStorage meatStorage, IVegitablesStorage vegitablesStorage) {
		this.meatStorage = meatStorage;
		this.vegitablesStorage = vegitablesStorage;

		setTitle("Add Meal");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		int width = 700;
		int height = 600;

		setSize(width, height);
		setMinimumSize(new Dimension(1000, 600));
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		setBounds(x, y, width, height);

		mealsTableModel = new ProductsTableModel();
		mealsTable = new JTable(mealsTableModel);

		setLayout(new BorderLayout());
		mealsType = new JComboBox<>(new ComboBoxModel<String>() {

			private MealType[] mealTypeCollection = { MealType.MEAT, MealType.VEGITABLES };

			private MealType selection = null;

			@Override
			public void addListDataListener(ListDataListener arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public String getElementAt(int index) {
				return mealTypeCollection[index].toString();
			}

			@Override
			public int getSize() {
				return mealTypeCollection.length;
			}

			@Override
			public void removeListDataListener(ListDataListener arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public Object getSelectedItem() {
				return selection;
			}

			@Override
			public void setSelectedItem(Object object) {
				String item = (String) object;
				switch (item) {
				case "MEAT":
					selection = MealType.MEAT;
					break;
				case "VEGITABLES":
					selection = MealType.VEGITABLES;
					break;
				default:
					break;
				}
			}
		});

		mealsType.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch ((MealType) mealsType.getSelectedItem()) {
				case MEAT:
					try {
						Collection<Product> collection = meatStorage.getMeatProducts();
						mealsTableModel.replaceData(new ArrayList<Product>(collection));
					} catch (MalformedURLException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
					break;
				case VEGITABLES:
					try {
						Collection<Product> collection = vegitablesStorage.getVegitablesProducts();
						mealsTableModel.replaceData(new ArrayList<Product>(collection));
					} catch (MalformedURLException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
				default:
					break;
				}

			}
		});

		addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				productAdded();
			}
		});

		add(mealsType, BorderLayout.NORTH);
		add(new JScrollPane(mealsTable), BorderLayout.CENTER);
		add(addButton, BorderLayout.SOUTH);
		pack();
	}

	public void addListener(IProductAddedListener listener) {
		listeners.add(listener);
	}

	private void productAdded() {
		Product product = mealsTableModel.get(mealsTable.getSelectedRow());
		for (IProductAddedListener listener : listeners) {
			listener.actionPerformed(product);
		}
	}

}
