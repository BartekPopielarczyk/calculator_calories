package com.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.common.IProductAddedListener;
import com.models.Product;

@Component("mainWindow")
public class MainWindow extends JFrame {
	private GridBagConstraints constains;
	private BmrPanel bmrPanel;
	private MealsPanel mealsPanel;

	private ChooseMealWindow chooseMealWindow;

	@Autowired
	public MainWindow(MealsPanel mealsPanel) {

		this.chooseMealWindow = chooseMealWindow;

		setTitle("Main Window");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		int width = 1000;
		int height = 600;

		setSize(width, height);
		setMinimumSize(new Dimension(1000, 600));
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		setBounds(x, y, width, height);

		constains = new GridBagConstraints();
		constains.weightx = 1;
		constains.weighty = 1;
		setLayout(new BorderLayout());

		bmrPanel = new BmrPanel();
		mealsPanel = mealsPanel;
		add(bmrPanel, BorderLayout.PAGE_START);
		add(mealsPanel, BorderLayout.CENTER);
		pack();
	}

	protected void addComponent(JPanel component, int gridx, int gridy, int gridwidth) {
		constains.fill = GridBagConstraints.BOTH;
		constains.gridx = gridx;
		constains.gridy = gridy;
		constains.gridwidth = gridwidth;
		constains.gridheight = 1;
		add(component, constains);
	}
}
