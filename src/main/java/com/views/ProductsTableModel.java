package com.views;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import org.springframework.stereotype.Component;

import com.models.Product;

@Component("productsTableModel")
public class ProductsTableModel extends AbstractTableModel {

	private ArrayList<Product> products;

	private String[] columnNames = { "LP", "Product Name", "Calories/100g", "Energy (KJ) / 100g", "Product Type" };

	public ProductsTableModel() {
		products = new ArrayList<>();
	}

	public Product get(int index) {
		return products.get(index);
	}

	public ArrayList<Product> getSelected() {
		ArrayList<Product> selectedItems = new ArrayList<>();
		for (Product person : products) {
			if (person.getIsSelected()) {
				selectedItems.add(person);
			}
		}

		return selectedItems;
	}

	public ArrayList<Product> getAll() {
		return products;
	}

	public void remove(int index) {
		products.remove(index);
		refresh();
	}

	public void remove(Product model) {
		products.remove(model);
		refresh();
	}

	public void replaceData(ArrayList<Product> collection) {
		products = collection;
		refresh();
	}

	public void add(Product person) {
		products.add(person);
		refresh();
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public int getRowCount() {
		if (products == null) {
			return 0;
		}
		return products.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Product product = products.get(row);

		switch (column) {
		case 0:
			return row + 1;
		case 1:
			return product.getProductName();
		case 2:
			return product.getCalories();
		case 3:
			return product.getKilojoules();
		case 4:
			return product.getType();
		default:
			return null;
		}
	}

	@Override
	public Class getColumnClass(int column) {
		switch (column) {
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return Integer.class;
		case 3:
			return Integer.class;
		case 4:
			return String.class;
		default:
			return String.class;
		}
	}

	private void refresh() {
		fireTableDataChanged();
	}

}
