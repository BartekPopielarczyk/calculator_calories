package com.views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;

import com.common.PhysicalActivity;
import com.common.Sex;
import com.services.bmr.BmrData;
import com.services.bmr.BmrResult;
import com.services.bmr.BmrService;
import com.services.bmr.IBmrData;
import com.services.bmr.IBmrResult;
import com.services.bmr.IBmrService;

public class BmrPanel extends JPanel {
	private GridBagConstraints constains;

	private JLabel ageLabel;
	private JLabel weightLabel;
	private JLabel heightLabel;
	private JLabel sexLabel;
	private JLabel physicalActivityLabel;
	private JComboBox sexComboBox;
	private JTextField age;
	private JTextField weight;
	private JTextField height;
	private JComboBox physicalActivityComboBox;
	private JButton calculateButton;
	private JLabel result;
	private JLabel bmrLabel;

	private IBmrService bmrService;

	public BmrPanel() {

		bmrService = new BmrService();

		setBorder(new EmptyBorder(20, 20, 20, 20));
		constains = new GridBagConstraints();
		constains.weightx = 1;
		constains.weighty = 1;
		setLayout(new GridBagLayout());

		sexLabel = new JLabel("Sex: ");
		sexComboBox = CreateSexCombobox();

		addComponent(sexLabel, 0, 0, 1);
		addComponent(sexComboBox, 0, 1, 2);

		ageLabel = new JLabel("Age: ");
		age = new JTextField();

		addComponent(ageLabel, 0, 2, 1);
		addComponent(age, 0, 3, 2);

		weightLabel = new JLabel("Weight: ");
		weight = new JTextField();
		addComponent(weightLabel, 0, 4, 1);
		addComponent(weight, 0, 5, 2);

		heightLabel = new JLabel("Height: ");
		height = new JTextField();
		addComponent(heightLabel, 0, 6, 1);
		addComponent(height, 0, 7, 2);

		physicalActivityLabel = new JLabel("Physical activity: ");
		physicalActivityComboBox = createPhysicalAvtivateCombobox();
		addComponent(physicalActivityLabel, 0, 8, 1);
		addComponent(physicalActivityComboBox, 0, 9, 2);

		calculateButton = new JButton("Calculate");
		calculateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				IBmrData data = new BmrData((Sex) sexComboBox.getSelectedItem(), new Integer(height.getText()),
						new Integer(weight.getText()), new Integer(age.getText()),
						(PhysicalActivity) physicalActivityComboBox.getSelectedItem());
				IBmrResult result = bmrService.calculate(data);

				bmrLabel.setText("Your caloric needs is: " + result.getCaloriesValue() + ", Proteins: "
						+ result.getProteinValue() + ", Fat: " + result.getFatValue() + ", Carbohydrates: "
						+ result.getCarbohydratesValue());
			}
		});

		addComponent(calculateButton, 0, 11, 1);

		result = new JLabel("Your BMR:");
		result.setBorder(new EmptyBorder(10, 50, 10, 10));
		bmrLabel = new JLabel("");
		bmrLabel.setBorder(new EmptyBorder(10, 50, 10, 10));

		addComponent(result, 1, 0, 4);
		addComponent(bmrLabel, 1, 1, 4);
	}

	private JComboBox createPhysicalAvtivateCombobox() {
		return new JComboBox<>(new ComboBoxModel<String>() {

			private PhysicalActivity[] physicalActivityCollection = { PhysicalActivity.LACK_OF_EXERCISES,
					PhysicalActivity.SMALL_ACTIVITY, PhysicalActivity.MEDIUM_ACTIVITY, PhysicalActivity.BIG_ACTIVITY,
					PhysicalActivity.LARGE_ACTIVITY, };

			private PhysicalActivity selection = null;

			@Override
			public void addListDataListener(ListDataListener arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public String getElementAt(int index) {
				return physicalActivityCollection[index].toString();
			}

			@Override
			public int getSize() {
				return physicalActivityCollection.length;
			}

			@Override
			public void removeListDataListener(ListDataListener arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public Object getSelectedItem() {
				return selection;
			}

			@Override
			public void setSelectedItem(Object object) {
				String item = (String) object;
				switch (item) {
				case "LACK_OF_EXERCISES":
					selection = PhysicalActivity.LACK_OF_EXERCISES;
					break;
				case "SMALL_ACTIVITY":
					selection = PhysicalActivity.SMALL_ACTIVITY;
					break;
				case "MEDIUM_ACTIVITY":
					selection = PhysicalActivity.MEDIUM_ACTIVITY;
					break;
				case "BIG_ACTIVITY":
					selection = PhysicalActivity.BIG_ACTIVITY;
					break;
				case "LARGE_ACTIVITY":
					selection = PhysicalActivity.LARGE_ACTIVITY;
					break;
				default:
					break;
				}
			}

		});
	}

	private JComboBox<String> CreateSexCombobox() {
		return new JComboBox<>(new ComboBoxModel<String>() {

			private Sex[] sexCollection = { Sex.MEN, Sex.WOMEN };
			private Sex selection = null;

			@Override
			public void addListDataListener(ListDataListener arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public String getElementAt(int index) {
				return sexCollection[index].toString();
			}

			@Override
			public int getSize() {
				return sexCollection.length;
			}

			@Override
			public void removeListDataListener(ListDataListener arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public Object getSelectedItem() {
				return selection;
			}

			@Override
			public void setSelectedItem(Object object) {
				String item = (String) object;
				if (item == "MEN") {
					selection = Sex.MEN;
				} else if (item == "WOMEN") {
					selection = Sex.WOMEN;
				}
			}
		});
	}

	protected void addComponent(java.awt.Component component, int gridx, int gridy, int gridwidth) {
		constains.fill = GridBagConstraints.HORIZONTAL;
		constains.gridx = gridx;
		constains.gridy = gridy;
		constains.gridwidth = gridwidth;
		add(component, constains);
	}
}
