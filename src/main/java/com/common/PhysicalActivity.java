package com.common;

public enum PhysicalActivity {
	LACK_OF_EXERCISES(1.0), SMALL_ACTIVITY(1.2), MEDIUM_ACTIVITY(1.4), BIG_ACTIVITY(1.6), LARGE_ACTIVITY(1.8);

	private double numVal;

	PhysicalActivity(double numVal) {
		this.numVal = numVal;
	}

	public double getNumVal() {
		return numVal;
	}

}
