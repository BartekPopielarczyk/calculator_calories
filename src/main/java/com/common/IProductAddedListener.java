package com.common;

import com.models.Product;

public interface IProductAddedListener {
	void actionPerformed(Product product);
}
