package com.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.springframework.stereotype.Component;

@Component("dataProvider")
public class DataProvider implements IDataProvider {

	@Override
	public String getData(URL url) {
		URLConnection urlConnection;
		StringBuilder result = new StringBuilder();
		String tmp;
		try {
			urlConnection = url.openConnection();
			urlConnection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((tmp = bufferedReader.readLine()) != null) {
				result.append(tmp + "\n");
			}

			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

}
