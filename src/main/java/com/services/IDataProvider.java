package com.services;

import java.net.URL;

public interface IDataProvider {

	String getData(URL url);
}
