package com.services;

import java.util.Collection;

import com.models.Product;
import com.views.ProductsTableModel;

public interface IFoodService {

	ProductsTableModel getTableModel();

	Collection<Product> getMeat();

	Collection<Product> getVegitables();
}
