package com.services;

import java.util.Collection;

public interface IParser<T> {
	Collection<T> parse(String text);
}
