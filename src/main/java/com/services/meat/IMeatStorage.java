package com.services.meat;

import java.net.MalformedURLException;
import java.util.Collection;

import com.models.Product;

public interface IMeatStorage {
	
	Collection<Product> getMeatProducts() throws MalformedURLException;
}
