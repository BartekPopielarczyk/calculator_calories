package com.services.meat;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.common.ProductType;
import com.models.Product;
import com.services.IDataProvider;
import com.services.IParser;

@Component("meatStorage")
public class MeatStorage implements IMeatStorage {

	private final String urlPath = "http://www.tabelakalorii.net/zywnosc/mieso-i-produkty-miesne/";

	private IDataProvider dataProvider;
	private IParser<Product> parser;
	private Collection<Product> internalProducts = new ArrayList<>();

	@Autowired
	public MeatStorage(IDataProvider dataProvider, IParser<Product> parser) {
		this.dataProvider = dataProvider;
		this.parser = parser;
	}

	@Override
	public Collection<Product> getMeatProducts() throws MalformedURLException {
		if (internalProducts.size() > 0) {
			return internalProducts;
		}
		String html = dataProvider.getData(new URL(urlPath));
		Collection<Product> meats = parser.parse(html);
		for (Product product : meats) {
			product.setType(ProductType.MEAT);
			internalProducts.add(product);
		}
		return meats;
	}
}
