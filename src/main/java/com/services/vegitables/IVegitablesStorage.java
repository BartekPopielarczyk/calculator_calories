package com.services.vegitables;

import java.net.MalformedURLException;
import java.util.Collection;

import com.models.Product;

public interface IVegitablesStorage {
	Collection<Product> getVegitablesProducts() throws MalformedURLException;
}
