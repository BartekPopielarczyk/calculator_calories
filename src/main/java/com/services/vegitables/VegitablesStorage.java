package com.services.vegitables;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.common.ProductType;
import com.models.Product;
import com.services.IDataProvider;
import com.services.IParser;

@Service("vegitablesStorage")
public class VegitablesStorage implements IVegitablesStorage {

	private final String urlPath = "http://www.tabelakalorii.net/zywnosc/warzywa/";

	private IDataProvider dataProvider;
	private IParser<Product> parser;
	private Collection<Product> internalProducts = new ArrayList<>();

	@Autowired
	public VegitablesStorage(IDataProvider dataProvider, IParser<Product> parser) {
		this.dataProvider = dataProvider;
		this.parser = parser;
	}

	@Override
	public Collection<Product> getVegitablesProducts() throws MalformedURLException {
		if (internalProducts.size() > 0) {
			return internalProducts;
		}
		String html = dataProvider.getData(new URL(urlPath));
		Collection<Product> meats = parser.parse(html);
		for (Product product : meats) {
			product.setType(ProductType.VEGETABLES);
			internalProducts.add(product);
		}
		return meats;
	}

}
