package com.services;

import java.net.MalformedURLException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.models.Product;
import com.services.meat.IMeatStorage;
import com.views.ProductsTableModel;

@Service("foodService")
public class FoodService implements IFoodService {

	private IMeatStorage meatStorage;
	private ProductsTableModel tableModel;

	@Autowired
	public FoodService(ProductsTableModel tableModel, IMeatStorage meatStorage) {

		this.meatStorage = meatStorage;
		this.tableModel = tableModel;
	}

	@Override
	public Collection<Product> getMeat() {
		Collection<Product> result = null;
		try {
			result = meatStorage.getMeatProducts();
		} catch (MalformedURLException e) {

		}
		return result;
	}

	@Override
	public Collection<Product> getVegitables() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductsTableModel getTableModel() {
		// TODO Auto-generated method stub
		return null;
	}

}
