package com.services;

import java.util.ArrayList;
import java.util.Collection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.models.Product;

@Service("productParser")
public class ProductParser implements IParser<Product> {

	@Override
	public Collection<Product> parse(String text) {
		ArrayList<Product> products = new ArrayList<>();
		Document doc = Jsoup.parse(text);
		Element table = doc.select("table#calories-table").get(0);
		Elements rows = table.select("tr");

		for (Element element : rows.subList(1, rows.size())) {
			Product product = new Product();
			Elements columns = element.select("td");

			product.setProductName(columns.get(0).text());
			String calories = columns.get(3).select("data").get(0).text();
			product.setCalories(Integer.parseInt(calories));
			String jules = columns.get(4).select("data").get(0).text();
			product.setKilojoules(Integer.parseInt(jules));

			products.add(product);
		}

		return products;
	}

}
