package com.services.bmr;

import org.springframework.stereotype.Service;

@Service("bmrService")
public class BmrService implements IBmrService {

	@Override
	public IBmrResult calculate(IBmrData data) {
		int calories = (int) (data.getWeight() * 24 * data.getActivity().getNumVal());
		int proteins = calories * 30 / 100;
		int fat = calories * 30 / 100;
		int carbo = calories * 40 / 100;

		return new BmrResult(calories, proteins, fat, carbo);
	}

}
