package com.services.bmr;

public class BmrResult implements IBmrResult {

	public BmrResult(int caloriesValue, int proteinValue, int fatValue, int carbohydratesValue) {
		super();
		this.caloriesValue = caloriesValue;
		this.proteinValue = proteinValue;
		this.fatValue = fatValue;
		this.carbohydratesValue = carbohydratesValue;
	}

	private int caloriesValue;
	private int proteinValue;
	private int fatValue;
	private int carbohydratesValue;

	@Override
	public int getCaloriesValue() {
		return caloriesValue;
	}

	@Override
	public int getProteinValue() {
		return proteinValue;
	}

	@Override
	public int getFatValue() {
		return fatValue;
	}

	@Override
	public int getCarbohydratesValue() {
		return carbohydratesValue;
	}
}
