package com.services.bmr;

import com.common.PhysicalActivity;
import com.common.Sex;

public class BmrData implements IBmrData {

	private Sex sex;
	private int height;
	private int weight;
	private int age;
	private PhysicalActivity activity;

	public BmrData(Sex sex, int height, int weight, int age, PhysicalActivity activity) {
		super();
		this.sex = sex;
		this.height = height;
		this.weight = weight;
		this.age = age;
		this.activity = activity;
	}

	public Sex getSex() {
		return sex;
	}

	public int getHeight() {
		return height;
	}

	public int getWeight() {
		return weight;
	}

	public int getAge() {
		return age;
	}

	public PhysicalActivity getActivity() {
		return activity;
	}

}
