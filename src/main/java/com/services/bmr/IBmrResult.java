package com.services.bmr;

public interface IBmrResult {

	int getCaloriesValue();

	int getProteinValue();

	int getFatValue();

	int getCarbohydratesValue();

}