package com.services.bmr;

import com.common.PhysicalActivity;
import com.common.Sex;

public interface IBmrData {

	Sex getSex();

	int getHeight();

	int getWeight();

	int getAge();

	PhysicalActivity getActivity();

}