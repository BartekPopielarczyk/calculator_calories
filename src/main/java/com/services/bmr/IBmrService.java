package com.services.bmr;

public interface IBmrService {
		IBmrResult calculate(IBmrData data);
}
