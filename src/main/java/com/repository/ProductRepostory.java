package com.repository;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.stereotype.Repository;

import com.models.Product;

@Repository("productRepostory")
public class ProductRepostory implements IRepository<Product> {

	@Override
	public int Add(Product entity) {
		Session session = getSession();
		int result = 0;
		final Transaction transaction = session.beginTransaction();
		try {
			result = (Integer) session.save(entity);
			Hibernate.initialize(result);
			transaction.commit();
		} catch (Exception ex) {
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
			return result;
		}
	}

	@Override
	public void Remove(Product entity) {
		Session session = getSession();
		final Transaction transaction = session.beginTransaction();
		try {
			session.delete(entity);
			transaction.commit();
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
		}
		
	}

	@Override
	public void Remove(int id) {
		Session session = getSession();
		final Transaction transaction = session.beginTransaction();
		try {
			session.delete(id);
			transaction.commit();
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
		}
		
	}

	@Override
	public Collection<Product> Get() {
		Session session = getSession();
		Collection<Product> result = null;
		final Transaction transaction = session.beginTransaction();
		try {
			result = (Collection<Product>) session.createCriteria(Product.class).list();
			Hibernate.initialize(result);
			transaction.commit();
		} catch (Exception ex) {
			
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
			return result;
		}
	}
	
	private Session getSession() {
		return HibernateUtils.getSessionFactory().openSession();
	}
}
