package com.repository;

import java.util.Collection;

public interface IRepository<T> {

	int Add(T entity);

	void Remove(T entity);

	void Remove(int id);

	Collection<T> Get();
}
